---
title: self introduction
tags:
  - viva
---
### Q: Tell me about yourself // introduce yourself
[Three Part Formula](notes/202309182356.md)

Assalamualaikum / thank you sir. My name is [Md. Jaynul Abedin Nishat](notes/202309302314.md) and I am coming from the northern district of Naogaon.

I have been working as a senior officer at Rupali Bank Limited, a state-owned commercial bank, for last 3 months. Before that I also worked as Senior Officer at Ansar-VDP Unnayan Bank for 6 months. Right after graduation, I also started working as a chief-technical officer of a rajshahi-based engineering consultancy startup named Photosphere for a brief period of time.

I did my undergraduate in Mechanical Engineering from Rajshahi University of Engineering and Technology, where it first struck to my mind that I want to work in public sector, especially in civil service. My desire to serve my countrymen by being part of the state-machine and the public institutions that had brought myself up till now has grew even bigger when I started working at the banks.

I believe my academic background in engineering together with my experience in financial institutions make me quite adaptable to various situations in highly responsible positions like the ones in the cadre service.

I also think of myself as a very keen learner and reader. I like to read to learn. My most favourite genre is thus Non-Fiction. But I am also efficient in learning from media other than text, while reading fictions is also one of my favourite pasttime.


আসসালামুআলাইকুম / ধন্যবাদ স্যার। আমার নাম মোঃ জয়নুল আবেদীন নিশাত আর আমার বাড়ি উত্তরের বরেন্দ্রভূমির নওগাঁ জেলায়। 

আমি গত ৩ মাস যাবত রাষ্ট্রমালিকানাধীন রূপালী ব্যাংক লিমিটেড এ সিনিয়র অফিসার হিসেবে কর্মরত আছি। এর পূর্বে আমি একই পদে আনসার-ভিডিপি উন্নয়ন ব্যাংকে ৬ মাস কর্মরত ছিলাম। এছাড়া স্নাতক শেষে বেশ কিছুদিন আমি ক্যাম্পাস ভিত্তিক একটি স্টার্টআপে চিফ টেকনিকাল অফিসার হিসেবে কাজ করেছি।

আমি রাজশাহী প্রকৌশল ও প্রযুক্তি বিশ্ববিদ্যালয় থেকে যন্ত্রকৌশল এ স্নাতক সম্পন্ন করি। বিশ্ববিদ্যালয়ে থাকাকালীনই আমার মধ্যে পাবলিক সেক্টরে কাজ করার ব্যাপারে ব্যাপক আগ্রহ জন্মায়। পরবর্তীতে কর্মক্ষেত্রে প্রবেশের পর এ ইচ্ছা আরও তীব্রতর হয়। ছোট থেকে এত বড় হওয়া পর্যন্ত জীবনের প্রতিটি ধাপেই সরকার ও রাষ্ট্র-যন্ত্র ওতপ্রোতভাবে জড়িত, ফলে এর চেয়ে সম্মানজনক ও উঁচুদরের কর্মক্ষেত্র খুব কমই পাওয়া যাবে, এ বোধোদয়ই আমার ইচ্ছার পেছনের কারণ। 

আমি মনে করি আমার প্রকৌশলে একাডেমিক ব্যাকগ্রাউন্ড ও আর্থিক প্রতিষ্ঠানে কাজের অভিজ্ঞতা ক্যাডার সার্ভিসের মত দায়িত্বশীল পদের জন্য খুবই কার্যকর হবে। 

এছাড়া অবসরে আমি বই পড়তে খুবি পছন্দ করি।